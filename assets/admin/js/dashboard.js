 $(document).ready(function() {
        jQuery(document).ready(function($) {
            
            $('#other_reason_main').hide();
            $('#other_answer_main').hide();
            $('#other_reason_note_main').hide();
            $('#other_answer_note_main').hide();
            $('#form').hide();
            $('#replace_ord_num').hide();

            $('#start-input').on('click', function(event) {
                event.preventDefault();
                $('#form').slideDown('slow/400/fast');
                // $("#start_time").text();
                // $('#start_time').val($.now());
                // $('#start_time').val(Date.now());
                 $('#start_time').val(moment().format('YYYY-MM-DD HH:mm:ss'));
                
                /* Act on the event */
            });

            // Event call when the cancel input event is called and get the start time and end time and make an ajax call to save this data
            $('#cancel-input').on('click', function(event) {
                event.preventDefault();
                $.ajax({
                    url: site_url + 'admin/cancel_request',
                    type: 'GET',
                    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                    data: {start_time: $('#start_time').val(), end_time: moment().format('YYYY-MM-DD HH:mm:ss'),cancel_request:true},
                })
                .done(function(data) {
                     window.location.reload(); 
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
                // alert("Start time:" + Date($('#start_time').val() * 1000) + " End Time:" + Date($.now())) ;
                
            });

            // Order number max key length check
            $('#order_num').keypress(function (){
                 if ($('#order_num').val().charAt(0) == '1' ) {
                    $('#order_num').attr('maxlength', '10');
                }
                else if($('#order_num').val().charAt(0).toLowerCase() == 'a' && $('#order_num').val().charAt(1).toLowerCase() == 'd'){
                     $('#order_num').attr('maxlength', '11');
                }
            });

            // submit data from the form to the server ajax call
            $('#submit').on('click', function(event) {
                event.preventDefault();
                var replace_order = $('#replace_order_num').val();
                if (replace_order.charAt(0).toLowerCase() == '1'){
                    //console.log($('#form').serialize());
                    save();
                }
                else if(replace_order.charAt(0).toLowerCase() == 'a' && replace_order.charAt(1).toLowerCase() == 'd'){
                    //console.log($('#form').serialize());
                    save();
                }
                else if(replace_order != ''){
                    alert('Incorrect Repleacement Order');
                    $('#replace_order_num').focus().select();
                }
                else{
                    //console.log($('#form').serialize());
                    save();
                }
                /* Act on the event */
            });

            function save(){
                $.ajax({
                    url: site_url + "admin/save",
                    type: 'GET',
                    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                    data: $('#form').serialize(),
                })
                .done(function(data) {
                    if (data == 1) {
                        window.location.reload();
                    };
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
                
            }

            // Replaecment order number max length check
            $('#replace_order_num').keypress(function (){
                 if ($('#replace_order_num').val().charAt(0) == '1' ) {
                    $('#replace_order_num').attr('maxlength', '10');
                }
                else if($('#replace_order_num').val().charAt(0).toLowerCase() == 'a' && $('#replace_order_num').val().charAt(1).toLowerCase() == 'd'){
                     $('#replace_order_num').attr('maxlength', '11');
                }
            });

            $('#category').on('click', function(event) {
                event.preventDefault();
                if ($('#order_num').val().charAt(0) == '1' ) {
                    if($('#order_num').val().charAt(0) == '1' && $('#order_num').val().length === 10 ){
                        // console.log('valid order number starting with 1');
                        $('#order_num').attr('readonly', '');
                    }
                    else{
                        alert('Order number must be 10 characters');
                        $('#order_num').focus();    
                        $('#order_num').select();    
                    }
                }
                else if($('#order_num').val().charAt(0).toLowerCase() == 'a' && $('#order_num').val().charAt(1).toLowerCase() == 'd'){
                     if($('#order_num').val().charAt(0).toLowerCase() == 'a' && $('#order_num').val().charAt(1).toLowerCase() == 'd' && $('#order_num').val().length === 11 ){
                        // console.log('valid order number starting with ad');
                        $('#order_num').attr('readonly', '');
                    }
                    else{
                        alert('Order number must be 11 characters');
                        $('#order_num').focus().select();    
                    }
                }
                else{
                    alert('Order should start with 1 or AD');
                    $('#order_num').focus().select();
                }
                
                /* Act on the event */
            });

            $('#category').on('change', function(event) {
                event.preventDefault();
                 // removing the values for from other column
               $('#other_reasons').val('');
               $('#other_answer').val('');
               $('#reasons').val('');
               $('#answers').val('');
                
                switch ($('#category option:selected').text()) {
                    case 'OTHERS':
                        // console.log('only the other options');
                        $('#other_reason_main').slideDown('slow/400/fast');
                        $('#other_answer_main').slideDown('slow/400/fast');
                        $('#reasons_all').slideUp('slow/400/fast');
                        $('#answers_all').slideUp('slow/400/fast');
                        break;
                    case 'YOUR ACCOUNT':
                        // console.log('only the other options');
                        $('#other_reason_main').slideDown('slow/400/fast');
                        $('#other_answer_main').slideDown('slow/400/fast');
                        $('#reasons_all').slideUp('slow/400/fast');
                        $('#answers_all').slideUp('slow/400/fast');
                        break;
                    default:
                        // console.log('all the others');
                        reasons_ajax($(this).val());
                        $('#other_reason_main').slideUp('slow/400/fast');
                        $('#other_answer_main').slideUp('slow/400/fast');
                        $('#reasons_all').slideDown('slow/400/fast');
                        $('#answers_all').slideDown('slow/400/fast');
                        break;
                }

            });
            $('#answers').on('change', function(event) {
                event.preventDefault();
                // if($('#answers :selected').val() == "STOP AND RETURN AND 500 OR REREQUEST"){
                //     alert('500 raised show a new order box');
                // }
                // $('#answers option:selected').text() == "STOP AND RETURN AND 500 OR REREQUEST"
                // console.log($('#answers option:selected').text().match('500'));
                if ($('#answers option:selected').text().match('500')) {
                    $('#replace_ord_num').slideDown('slow/400/fast');
                    // console.log("show to replacement order box");
                }
                /* Act on the event */
            });

            $('#reasons').on('change', function(event) {
                event.preventDefault();
                $('#other_note_reasons').val('');
                $('#other_note_answer').val('');
                 if ($('#reasons option:selected').text().toUpperCase() == "OTHERS") {
                    $('#other_reason_main').slideDown('slow/400/fast');
                    $('#other_answer_main').slideDown('slow/400/fast');
                    $('#answers_all').slideUp('slow/400/fast');
                }
                else{
                    $('#other_reason_main').slideUp('slow/400/fast');
                    $('#other_answer_main').slideUp('slow/400/fast');
                    $('#answers_all').slideDown('slow/400/fast');
                    solutions_ajax($(this).val());    
                }
                
                /* Act on the event */
            });

            function reasons_ajax(cate_id){
                 $.ajax({
                        url: site_url +"admin/reasons",
                        type: 'POST',
                        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                        data: {category_id: cate_id},
                    })
                    .done(function(data) {
                        if (data != 0) {
                            
                            $('#reasons').children().remove();
                            $('#answers').children().remove();
                            $('#reasons').append('<option value="">Select Reasons</option>');
                            $.each(jQuery.parseJSON(data), function(index, val) {
                                 $('#reasons').append("<option value="+val.id+">"+val.name+"</option>");
                            });
                            

                        };
                    });
            }

            function solutions_ajax(reas_id){
                 $.ajax({
                        url: site_url+"admin/solutions",
                        type: 'POST',
                        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                        data: {reason_id: reas_id},
                    })
                    .done(function(data) {
                        if (data != 0) {
                            
                            $('#answers').children().remove();
                            $('#answers').append('<option value="">Select Answers</option>');
                            $.each(jQuery.parseJSON(data), function(index, val) {
                                 $('#answers').append("<option value="+val.id+">"+val.name+"</option>");
                            });
                            

                        };
                    });
            }
            
        });
    });     