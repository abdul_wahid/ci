<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	public $data = array();
	public function __construct()
	{
		parent::__construct();
		
		// this define the access for the users based on the url
		$exception_uris = array('','admin/index','admin','users/logout','users/login');
		
		 if(in_array( uri_string(), $exception_uris) == FALSE ){
	        if(!$this->ion_auth->logged_in()){
	          redirect('');
	        }
	      }
		
	}

	public function index()
	{
		
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */