<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function index(){
		// show admin login panel
		$this->data['modules'] = 'admin';
		$this->data['sub_view'] = 'index';

		$this->load->view('index');
		// echo Modules::run('templates/admin',$this->data);
	}



	public function dashboard()
	{
		$this->data['modules'] = 'admin';
		$this->data['sub_view'] = 'dashboard';

		echo Modules::run('templates/admin',$this->data);
	}

	public function blank(){

		$this->data['modules'] = 'admin';
		$this->data['sub_view'] = '_blank';

		echo Modules::run('templates/admin',$this->data);
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */