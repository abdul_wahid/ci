<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Thumbnails_generate extends MX_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	// public function index($source_picture){
	// 	// Create the array various image sizes
	// 	// Maintain ratio if set to TRUE will lock the size to the given width and height
	// 	// By default maintain ratio is set to TRUE
	// 	$configs = array();
	// 	$configs[] = array('source_image' => 'motive.jpg', 'new_image' => '120/thumb.jpg', 'width' => 120, 'height' => 120);
	// 	$configs[] = array('source_image' => 'motive.jpg', 'new_image' => '120x120/thumb.jpg', 'width' => 120, 'height' => 120, 'maintain_ratio' => FALSE);
	// 	$configs[] = array('source_image' => 'motive.jpg', 'new_image' => '160x90/thumb.jpg', 'width' => 160, 'height' => 90, 'maintain_ratio' => FALSE);
	// 	$configs[] = array('source_image' => 'motive.jpg', 'new_image' => '240/thumb.jpg', 'width' => 240, 'height' => 240);
	// 	$configs[] = array('source_image' => 'motive.jpg', 'new_image' => '800/thumb.jpg', 'width' => 800, 'height' => 800);

	// 	// Loop through the array to create thumbs
	// 	$this->load->library('image_lib');
	// 	foreach ($configs as $config) {
	// 		$this->image_lib->thumb($config, FCPATH . 'assets/uploads/files/');
	// 	}
	// 	// Pass the config array to the view
	// 	$data = array('images' => $configs);
	// 	$this->load->view('index',$data);
	// }

	// public function upload(){
	// 	$this->load->view('upload');
	// }

    /**
     * sets the config array and upload path for the orignal image
     */
    private function set_upload_options()
    {   
        //upload an image options
        $config = array();
        $config['upload_path'] = FCPATH . 'assets/uploads/files';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = uniqid();

        return $config;
    }

    /**
     * upload the orignal pictures to assets/upload/files and then call the resize functions
     * @return array returns the array of picture titles 
     */
	public function upload_img(){
		// saves the names of the picture titles
		$picture_title = array();

		// call the upload codeigniter library
		$this->load->library('upload');

	    /**
	     * sets the $_FILES array for the pictures to be uploaded
	     * @var Golbal array
	     */
	    $files = $_FILES;
	    $cpt = count($_FILES['picture']['name']);
	    for($i=0; $i<$cpt; $i++){

	        $_FILES['picture']['name']= $files['picture']['name'][$i];
	        $_FILES['picture']['type']= $files['picture']['type'][$i];
	        $_FILES['picture']['tmp_name']= $files['picture']['tmp_name'][$i];
	        $_FILES['picture']['error']= $files['picture']['error'][$i];
	        $_FILES['picture']['size']= $files['picture']['size'][$i];

	        /**
	         * sets the uploaded array for serilized upload
	         */
		    $this->upload->initialize($this->set_upload_options());
		    if ( ! $this->upload->do_upload('picture')){
				$error = array('error' => $this->upload->display_errors());
				dump_exit($error);
			}
			else{
				$data = array('upload_data' => $this->upload->data());
				$this->resize($data['upload_data']['orig_name'],null,'home_slider',150,160,false);
				$this->resize($data['upload_data']['orig_name'],null,'large_slider',690,414,false);
				$this->resize($data['upload_data']['orig_name'],null,'thumb',100,100,false);
				$this->resize($data['upload_data']['orig_name'],null,'relative_ads',129,78,false);
				// $this->resize($data['upload_data']['orig_name'],null,'large_slider',690,414,false);
				// $this->home_slider($data['upload_data']['orig_name']);
				// $this->large_slider($data['upload_data']['orig_name']);
				// $this->thumb($data['upload_data']['orig_name']);
				// $this->relative_ads($data['upload_data']['orig_name']);
				$picture_title[] = $data['upload_data']['orig_name'];
			}
	    }
	    return $picture_title;
	}

	/**
	 * resize and image and save it to the appropreate folder, folder have to made before the saving can happen
	 * @param  string  $picture_soruce this is the name of the picture, after upload file_name
	 * @param  string  $folder         This is the main folder in which the picture has to be saved "Parent Folder", if null means that it is in the root folder
	 * @param  string  $resize_folder  This is the sub folder in which the picutre has to be saved "Child Folder"
	 * @param  integer $width          Width of the picture
	 * @param  integer $height         Height of the picutre
	 * @param  boolean $maintain_ratio To set the aspect ratio
	 * @return void                  
	 */
	public function resize($picture_soruce = null, $folder = null,$resize_folder = null, $width = 0,$height = 0,$maintain_ratio = FALSE){
		
		$configs[] = array('source_image' => $picture_soruce, 'new_image' => $resize_folder.'/'.$picture_soruce, 'width' => $width, 'height' => $height, 'maintain_ratio' => $maintain_ratio);
		
		$this->load->library('image_lib');
		foreach ($configs as $config) {
			$this->image_lib->thumb($config, FCPATH . 'assets/uploads/files/'.$folder.'/');
		}
	}

	public function home_slider($picture_soruce){
		$configs[] = array('source_image' => $picture_soruce, 'new_image' => 'home_slider/'.$picture_soruce, 'width' => 150, 'height' => 160, 'maintain_ratio' => FALSE);
		
		$this->load->library('image_lib');
		foreach ($configs as $config) {
			$this->image_lib->thumb($config, FCPATH . 'assets/uploads/files/');
		}
	}

	public function large_slider($picture_soruce){
		$configs[] = array('source_image' => $picture_soruce, 'new_image' => 'large_slider/'.$$picture_soruce, 'width' => 690, 'height' => 414, 'maintain_ratio' => FALSE);		
		
		$this->load->library('image_lib');
		foreach ($configs as $config) {
			$this->image_lib->thumb($config, FCPATH . 'assets/uploads/files/');
		}
	}

	public function thumb($picture_soruce){
		$configs[] = array('source_image' => $picture_soruce, 'new_image' => 'thumb/'.$$picture_soruce, 'width' => 100, 'height' => 100, 'maintain_ratio' => FALSE);		
		
		$this->load->library('image_lib');
		foreach ($configs as $config) {
			$this->image_lib->thumb($config, FCPATH . 'assets/uploads/files/');
		}
	}

	public function relative_ads($picture_soruce){
		$configs[] = array('source_image' => $picture_soruce, 'new_image' => 'relative_ads/'.$$picture_soruce, 'width' => 129, 'height' => 78, 'maintain_ratio' => FALSE);
		
		$this->load->library('image_lib');
		foreach ($configs as $config) {
			$this->image_lib->thumb($config, FCPATH . 'assets/uploads/files/');
		}
	}
}
/* End of file thumbnails_generate.php */
/* Location: ./application/controllers/thumbnails_generate.php */