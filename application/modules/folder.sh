mkdir $1
cd $1
mkdir controllers
mkdir models
mkdir views

cd controllers

touch $1.php

cd ..

cd models

touch $1_m.php

cd ..
cd views

touch index.php

