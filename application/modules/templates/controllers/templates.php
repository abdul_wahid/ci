<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Templates extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function home($data){

		$this->load->view('home/_layout_main', $data);
	}

	public function mobile($data){
		$this->load->view('mobile/_layout_main', $data);
	}

	public function admin($data){
		$this->load->view('admin/_layout_main', $data);	
	}

	public function left_sidebar(){
		$this->load->view('home/components/sidebar_comp_left');
	}

	public function right_sidebar(){
		$this->load->view('home/components/sidebar_comp_right');
	}

}

/* End of file Site.php */
/* Location: ./application/controllers/Site.php */