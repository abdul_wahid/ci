<?php $this->load->view('components/header.php'); ?>

<!-- wrapper start -->
<div class="container">
	<header class="container">
		<div class="row" style="  padding-top: 5px;">
			<div class="col-md-5 ">
				<a href="<?php echo site_url('home'); ?>"><img  data-original="<?php echo base_url('assets/uploads/files/'.$site_logo); ?>" class="logo img-responsive img-rounded pull-left"></a>
			</div>
			<div class="col-md-3 ">
				<img data-original="<?php echo base_url(); ?>img/af.png" class="pull-left img-resposive">
				<p class="cities-list pull-left"><a href="">Afghanistan <span class="glyphicon glyphicon-arrow-right"></span></a></p>
				<?php if($this->session->userdata('current_city_name') == 0): ?>
					<p style="z-index:0;">All Cities Selected</p>
				<?php elseif ($this->session->userdata('current_city_name') != ''): ?>

					<p style="z-index:0;"><?php echo  Modules::run('cities/get_city_session',$this->session->userdata('current_city_name'))->city_name; ?></p>
					
				<?php endif ?>
				<p></p>
				<ul class="cities" id="city_id">
					<?php foreach ($cities as $city): ?>
						<li><a href=""  class="cities_selection list-of-cities" data-city="<?php echo $city->id; ?>"><?php echo $city->city_name; ?></a></li>
					<?php endforeach ?>
				</ul>
			</div>
			<div class="col-md-4  pull-right">
				<ul class="signin" style="margin-right:-7px;">

					<!-- onclick="checkLoginState();" -->
					<?php if (!$this->ion_auth->logged_in()): ?>
						<!-- when not signed in -->
						<li><img data-original="<?php echo base_url(); ?>img/fbIcon.png" class="fb-login-button"></li>
						<li class="sign"><a href="#" ><?php echo $this->session->userdata('lang_current') == 'dr' ? 'ورود' : 'Sign in'; ?></a></li>
						<li><a href="<?php echo site_url('home/registration'); ?>"> <?php echo $this->session->userdata('lang_current') == 'dr' ? 'ثبت نام' : 'Register'; ?></a></li>
						<!-- when not signed in -->
					<?php else: ?>
						<!-- when signed in -->
						<li><a href="<?php echo site_url('users/user_profile'); ?>" ><?php echo $this->session->userdata('lang_current') == 'dr' ? 'خروج' : 'User Profile'; ?></a></li>
						<li><a href="<?php echo site_url('users/logout'); ?>" ><?php echo $this->session->userdata('lang_current') == 'dr' ? 'خروج' : 'Logout'; ?></a></li>
						<!-- when signed in -->
					<?php endif; ?>
					<?php if ($this->session->userdata('lang_current')== 'en'): ?>
						<li id="language" data-lang="dr"><img src="<?php echo base_url('img/flag-afghanistan.png');?>" class="flag-language" style="  width: 15px;" alt="flag"><a href=""> Dari</a></li>
					<?php else: ?>
						<li id="language" data-lang="en"><a href=""><img src="<?php echo base_url('img/flag-american.gif');?>" class="flag-language" style="  width: 15px;" alt="flag"> English</a></li>
					<?php endif ?>

				</ul>
					<a href="<?php echo site_url('home/place_ad'); ?>"><img data-original="<?php echo base_url(); ?>img/p-ad.gif" class="img-responsive img-rounded post-add"/></a>
			</div>
			<div class="col-md-12 nav" style="padding:0;"  <?php echo $this->session->userdata('lang_current') == 'dr' ? 'dir="RTL"' : ''; ?>>
				<nav class="navbar navbar-default" role="navigation">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?php echo site_url('home');?>"><span class="glyphicon glyphicon-home"></span></a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<?php $main = Modules::run('main_category/get_main'); ?>
							<?php foreach ($main as $m): ?>
							<?php $sub = Modules::run('sub_category/get_sub',$m->main_id); ?>
								<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $m->main_name; ?> <b class="caret"></b></a>
								<ul class="dropdown-menu dropdown-sub-menu-size">
									<?php foreach ($sub as $s): ?>
										<li><a href="<?php echo site_url('home/categories/'.strtolower(htmlspecialchars($m->url_main_name))."/".strtolower(htmlspecialchars($s->url_sub_name))); ?>"><?php echo $s->sub_name; ?></a></li>
									<?php endforeach; ?>
								</ul>
							</li>
							<?php endforeach; ?>
							<li>
								<a href="<?php echo site_url('company_jobs/jobs/'); ?>"><?php echo $this->session->userdata('lang_current') == 'dr' ? 'وظایف' : 'Jobs'; ?> <!-- <b class="caret"></b> --></a>
								<!-- <ul class="dropdown-menu">
								</ul> -->
							</li>
							<li>
								<a href="<?php echo site_url('company/home'); ?>"><?php echo $this->session->userdata('lang_current') == 'dr' ? 'شرکت ها' : 'Company'; ?> <!-- <b class="caret"></b> --></a>
								<!-- <ul class="dropdown-menu">
								</ul> -->
							</li>
							<li>
								<a href="<?php echo site_url('home/telecommunication'); ?>"><?php echo $this->session->userdata('lang_current') == 'dr' ? 'کارت موبائیل' : 'Top Up'; ?> <!-- <b class="caret"></b> --></a>
								<!-- <ul class="dropdown-menu">
								</ul> -->
							</li>
						</ul>
						
						</div><!-- /.navbar-collapse -->
					</nav>
				</div>
			</div>
		</header>
		<!-- content area -->


<?php $this->load->view($modules."/".$sub_view); ?>


<?php $this->load->view('components/footer.php'); ?>
<?php if ($this->session->flashdata('login_false') != ''): ?>
	<script>alert("<?php echo $this->session->flashdata('login_false'); ?>");</script>
<?php endif ?>
