<div class="col-md-2">
	<h4><?php echo $this->session->userdata('lang_current') == 'dr' ? 'فهرست شرکت ها' : 'Companies List'; ?></h4>
	<ul class="list-group">
		<?php foreach ($comp_cate as $cate): ?>
			  <li class="list-group-item">
			    <a href="<?php echo site_url('company/home/'.$cate->comp_name); ?>"><?php echo $cate->comp_name; ?></a>
			    <span class="glyphicon glyphicon-chevron-right"></span>
			  </li>
		<?php endforeach;?>
	</ul>
	
</div>