<!-- this div is for the div.container in the _layout_main file starting end div -->
<style>
	footer#footer > a{
		color: #fff;
	}
	footer#footer > a:hover{
		color: #fff;
		text-decoration: none;
	}
</style>
</div>
<div id="footer-image" class="img-responsive" style="background-image: url('<?php echo base_url("assets/uploads/files/$site_footer_bg"); ?>');">&nbsp;</div>
<footer id="footer">
	<div class="row">
		<div class="col-md-12">
			<h5 class="pull-left">&copy; Osyar.com <?php echo Date('Y'); ?>, all right reserved</h5>
			<h5 class="pull-right">Design And Developed by Abdul Wahid</h5>
		</div>
		<div class="col-md-12">
			<div class="col-md-3">
				<h4><?php echo $this->session->userdata('lang_current') == 'dr' ? '' : 'Language'; ?></h4>
				<?php if ($this->session->userdata('lang_current')== 'en'): ?>
				<li id="language" class="btn text-center" data-lang="dr"><img src="<?php echo base_url('img/flag-afghanistan.png');?>" class="flag-language" style="  width: 15px;" alt="flag"><a href="" style="color:#fff; text-decoration:none;"> Dari</a></li>
				<?php else: ?>
				<li id="language" class="btn text-center" data-lang="en"><a href="" style="color:#fff; text-decoration:none;"><img src="<?php echo base_url('img/flag-american.gif');?>" class="flag-language" style="  width: 15px;" alt="flag"> English</a></li>
				<?php endif ?>
				<!-- <p id="language" data-lang="en">English</p>
				<p id="language" data-lang="dr">Dari</p> -->
			</div>
			<div class="col-md-3">
				<h4><?php echo $this->session->userdata('lang_current') == 'dr' ? 'دریافت اجتماعی' : 'Get Soical'; ?></h4>
				<p><a href="http://www.facebook.com/osyaronlinestore" target="_blank" style="color:#fff; text-decoration:none;"> Facebook</a></p>
				<p>Twitter</p>
				<p>Youtube</p>
			</div>
			<div class="col-md-3">
				<h4><?php echo $this->session->userdata('lang_current') == 'dr' ? 'همکاری' : 'Support'; ?></h4>
				<?php $quick_link = (Modules::run('quick_link/get')); ?>
				
				<?php foreach ($quick_link as $ql): ?>
					<a href="<?php echo site_url('home/quick_link/'.$ql->id); ?>"><p><?php echo  $ql->quick_link_name;?></p></a>
				<?php endforeach ?>
			</div>
			<div class="col-md-3">
				<h4><?php echo $this->session->userdata('lang_current') == 'dr' ? 'ولایات دیگر' : 'Other provinces'; ?></h4>
				<!--<?php //$cities = Modules::run('cities/get_cities'); ?>-->
				<select name="cities cities_selection" id="footer-cities" class="form-control">
					<?php foreach ($cities as $city): ?>
						<option value="<?php echo site_url($city->id); ?>" data-city-id='<?php echo $key; ?>'><?php echo $city->city_name; ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
	</div>
</footer>
<!-- wrapper end-->
<!-- -->
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/dropdown-cities.js"></script>
<script src="<?php echo base_url(); ?>js/accordin.js"></script>
<script src="<?php echo base_url(); ?>js/aside.js"></script>
<!-- slider script-->
<script src="<?php echo base_url(); ?>js/slider/sly.min.js"></script>
<script src="<?php echo base_url(); ?>js/slider/horizontal.js"></script>
<!-- bootstrap tooltip-->
<script>
	$(function (){
		$(window).resize(function() {
			/* Act on the event */
			if(jQuery(window).width()<1024){
				// redirect to mobile site
				window.location.replace("<?php echo site_url('mobile/home'); ?>");
			}
		});
		if(jQuery(window).width()<1024){
			window.location.replace("<?php echo site_url('mobile/home'); ?>");
		}
	});

	$(function() {
		$(document).ready(function() {
			$('select#footer-cities').on('change', function(event) {
				event.preventDefault();
				window.location.reload();
				/* Act on the event */
			});
			$('ul#city_id > li > a').on('click', function () {
				event.preventDefault();
				window.location.reload();
			});
		});
	});		
	$(function() {
		var target='ul.clearfix li a';
	$(target).on('mouseover', function(event) {
		event.preventDefault();
		/* Act on the event */
		$(this).tooltip('toggle');
	});
		$('ul.clearfix li.active img').data('original-title', 'this is working');
	var signin=$('#signin'),
		exit=$('span.glyphicon.glyphicon-off');
	signin.slideUp('fast');
	$('li.sign a').on('click', function(event) {
		event.preventDefault();
		signin.slideToggle('slow/400/fast');
	});
	exit.on('click', function(event) {
		event.preventDefault();
		signin.slideToggle('slow/400/fast');
	});
});
</script>
<?php if($this->uri->segment(2) == 'items'): ?>

<!-- Scripts -->

<?php elseif($this->uri->segment(2) == 'registration' || $this->uri->segment(1) == 'company_jobs' || $this->uri->segment(2) == 'paid_member_registration' || $this->uri->segment(2) == 'user_profile'): ?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/datepicker.css">
	<script src="<?php echo base_url('js/bootstrap-datepicker.js') ?>"></script>
	<script>
	$(function () {
		$('.datepicker').datepicker();
		$('.datepicker').on('click', function(event) {
			$(this).select();
			/* Act on the event */
		});
	});
	</script>
<?php endif; ?>

<script>
	$(function() {
		$(document).ready(function() {
			
			var size = parseInt($('#ci_profiler_memory_usage').children('div').text());
			var convert_kb = (size/1024);
			$('#ci_profiler_memory_usage').children('div').text(convert_kb + ' Size in Kb');
		});
	});
</script>

<script src="<?php echo base_url('js/jquery.lazyload.min.js'); ?>"></script>
<script>
	$(function() {
		$('img').addClass('lazy');
	});
</script>
<script type="text/javascript" charset="utf-8">
  $(function() {
     $("img.lazy").lazyload({
         // effect : "fadeIn",
         threshold : 200
     });

  });
  </script>

  <script type="text/javascript" charset="utf-8">
  	$(function() {
  		$(document).ready(function() {
  			$('.cities_selection').on('click', function(event) {
  				event.preventDefault();
  				$.ajax({
  					url: "<?php echo site_url('home/set_city');?>",
  					type: 'POST',
  					data: {city_id: $(this).data('city-id')},
  				})
  				.done(function(data) {
  					console.log("success");
  					console.log(data);
  					// $('ul.cities').toggle('hide');
  					$('ul.cities').addClass('hide');
  				})
  				.fail(function() {
  					console.log("error");
  				})
  				
  				/* Act on the event */
  			});
  			$('.cities_selection').on('change', function () {
  				alert('working');
  				event.preventDefault();
  				alert($(this).children('option').select().data('city-id'));
  			});
  			$('#language > a').on('click', function(event) {
  				event.preventDefault();
  				
  				// alert("<?php echo $this->session->userdata('lang_current');?>");
  				// if($('#language').data('lang') == 'en'){
  				// 	$('#language').data('lang', 'dr');
  				// 	$('#language > a').text('Dari');
  				// }
  				// else if($('#language').data('lang') == 'dr'){
  				// 	$('#language').data('lang', 'en');
  				// 	$('#language > a').text('English');
  				// }

  				$.ajax({
  					url: "<?php echo site_url('home/lang');?>",
  					type: 'POST',
  					dataType: '',
  					data: {lang: $('#language').data('lang')},
  				})
  				.done(function(data) {
  					console.log("success");
  					console.log(data);
  					window.location.reload(true);
  					// window.location.replace("<?php echo site_url('"+window.location.href+"'); ?>");
  				})
  				.fail(function(data) {
  					console.log("error");
  					console.log(data);
  				});
  				


  			});
  		});
  	});
  </script>

  <script>
		$(function() {
			$(document).ready(function() {
				$('a.list-of-cities').on('click', function(event) {
					event.preventDefault();
					
					/* Act on the event */
					$.ajax({
					url: "<?php echo site_url('home/set_city_session');?>",
					type: 'POST',
					data: {city_id: $(this).data('city')},
				})
				.done(function(data) {
					if(data == 'done'){
						window.location.reload();
					}
					else{
						
					}
				})
				.fail(function(data) {
					console.log("error:" + data);
				});
				});
			});
		});
		</script>
		<script>
		$(function() {
			$(document).ready(function() {
				// $('.dropdown-sub-menu-size').css('width', $('ul.nav.navbar-nav').width());
			});
		});
		</script>
</body>
</html>