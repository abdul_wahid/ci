<div class="col-md-2  aside" style="padding-left:3px; padding-right:0;">
					<form action="#" method="POST" id="form-search" role="form">
						<!-- change the option based on the catagories-->
						<legend><b class="glyphicon glyphicon-search"></b>  <?php echo $this->session->userdata('lang_current') == 'dr' ? 'جستجو' : 'Search'; ?></legend>
						
						<div class="form-group">
							<select name="cities" class="form-control">
								<?php if ($this->session->userdata('current_city_name') == 0): ?>
									<option>All Cities</option>
								<?php endif ?>
								<?php foreach ($cities as $city): ?>
									<option value="<?php echo $city->id; ?>"><?php echo $city->city_name; ?></option>
								<?php endforeach ?>
							</select>
							<span class="glyphicon glyphicon-chevron-down"></span>
						</div>

						<div class="form-group">
							<select name="main" id="inputMain" class="form-control" data-aside="main-catagoires" id="main-catagoires">
								
							</select>
							<span class="glyphicon glyphicon-chevron-down main"></span>
						</div>
						<div class="form-group">
							<select name="sub" id="inputSub" class="form-control" data-aside="sub-catagoires">
								
							</select>
							<span class="glyphicon glyphicon-chevron-down sub"></span>
						</div>
						<!-- Depends on the catagoires selected start-->
						<!-- car products -->
						<div class="form-group">
							<select name="products" id="inputProducts" class="form-control">
								
							</select>
							<span class="glyphicon glyphicon-chevron-down product"></span>
						</div>
						<!-- car maker -->
						
						
						<!-- Depends on the catagoires selected end-->
						<div class="form-group">
							<label for="search">Keyword</label>
							<input type="search" class="form-control" id="search" placeholder="Search">
						</div>
						<button type="submit" class="btn form-submit form-control input-lg"><?php echo $this->session->userdata('lang_current') == 'dr' ? 'جستجو' : 'Search'; ?></button>
					</form>
					
					<div class="panel panel-danger quick-link">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo $this->session->userdata('lang_current') == 'dr' ? 'لینک سریع' : 'Quick Link'; ?> </h3>
						</div>
						<div class="panel-body">
							<ul>
								<?php foreach ($quick_link as $ql): ?>
									<li><a href="<?php echo site_url('home/quick_link/'.$ql->id); ?>"><?php echo  $ql->quick_link_name;?></a></li>
								<?php endforeach ?>
							</ul>
						</div>
					</div>

					<?php $paid_ads = Modules::run('paid_ads/get_paid_ads'); ?>
					<?php if (count($paid_ads)>0): ?>
						<?php foreach ($paid_ads as $ads): ?>
							<div class="panel panel-danger quick-link">
									<a href="<?php echo isset($ads->ads_link) == true ? $ads->ads_link : '#' ;?>"><img data-original="<?php echo base_url('assets/uploads/files/paid_ads/small_img/'.$ads->ads_image); ?>" alt=""></a>
							</div>
						<?php endforeach ;?>
					<?php endif; ?>
						
				</div>

<script>
	$(function() {
		$(document).ready(function() {
			$('#inputSub').hide();
			$('span.sub').hide();
			$('span.product').hide();
			$('#inputProducts').hide();

			$.ajax({
					url: "<?php echo site_url('fv/ajax_main'); ?>",
					type: 'POST',
					dataType: 'json',
					data: {name : $('#name').val()},
				})
				.done(function(data) {
					console.log("success");
					$('#inputMain').append('<option value="break">Main Category</option>');
					$.map(data, function(item, index) {
						$('#inputMain').append('<option value="'+ item.main_id +'">'+ item.main_name +'</option>');
					});
						
					// var mainName = jQuery.parseJSON(data);
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
				

			$('#inputMain').change(function(event) {
				/* Act on the event */
				if($(this).val() != "break"){
					$('#inputSub').find('option').remove();
					$('#inputSub').hide();
					$('span.sub').hide();

					$.ajax({
						url: "<?php echo site_url('fv/ajax_sub'); ?>",
						type: 'POST',
						dataType: 'json',
						data: {main_id: $(this).val(), ajax: true},
					})
					.done(function(data) {
						console.log("success");
						$('#inputSub').append('<option value="break">Sub Category</option>');
						$.map(data, function(item, index) {
							$('#inputSub').show();
							$('span.sub').show();
							$('#inputSub').append('<option value="'+ item.sub_id +'">'+ item.sub_name +'</option>');
						});
					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
						console.log("complete");
					});
				}
				
			});

			$('#inputSub').change(function(event) {
				/* Act on the event */
				if($(this).val() != "break"){
					// $('#inputSub').find('option').remove();
					$('#inputProducts').find('option').remove();
					$('#inputProducts').hide();
					$('span.product').hide();

					$.ajax({
						url: "<?php echo site_url('fv/ajax_products'); ?>",
						type: 'POST',
						dataType: 'json',
						data: {sub_id: $(this).val(), ajax: true},
					})
					.done(function(data) {
						console.log("success");
						$("#inputProducts").show();
						$('span.product').show();
						$('#inputProducts').append('<option value="break">Products</option>');


						$.map(data, function(item, index) {
							$('#inputProducts').append('<option value="'+ item.pro_id +'">'+ item.pro_name +'</option>');
						});
					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
						console.log("complete");
					});
				}
				
			});
		});
	});

	$(function() {
		$('#form-search').submit(function(event) {
				/* Act on the event */
				event.preventDefault();
				var main = $('#inputMain').val(),
					sub = $('#inputSub').val(),
					product = $('#inputProducts').val();
				if(main != 'break' && sub != 'break'){
					// The encodeURIComponent() function encodes a URI component.
					main = encodeURIComponent($('#inputMain option:selected').text()),
					sub = encodeURIComponent($('#inputSub option:selected').text());
					if(product !='break'){
						product = encodeURIComponent($('#inputProducts option:selected').text());
						// redirect to the url with the product in it
						// console.log("<?php echo site_url('home/categories'); ?>" +"/"+ main.toLowerCase() +"/"+ sub.toLowerCase() +"/"+ product.toLowerCase());
						window.location.replace("<?php echo site_url('home/categories'); ?>" +"/"+ main.toLowerCase() +"/"+ sub.toLowerCase() +"/"+ product.toLowerCase());
					}
					else{
						// redirect to the url with the product not in it
						window.location.replace("<?php echo site_url('home/categories'); ?>" +"/"+ main.toLowerCase() +"/"+ sub.toLowerCase());
					}
				}
			});
	});
</script>