<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Osyar <?php echo $this->uri->segment(2) !=''? '| '.$this->uri->segment(2): '| Home'; ?></title>
		
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/horizontal.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css">
		<?php if($this->session->userdata('lang_current') == 'dr'): ?>
			<link rel="stylesheet" href="<?php echo base_url('css/style_dr.css'); ?>">
		<?php endif; ?>
		<script src="<?php echo base_url(); ?>js/jquery.js"></script>
		<?php if($this->uri->segment(2) == '' || $this->uri->segment(2) == 'index'): ?>

			<script src="<?php echo base_url(); ?>js/slider/ga.js"></script>
			<script src="<?php echo base_url(); ?>js/slider/modernizr.js"></script>
			<script src="<?php echo base_url(); ?>js/respond.js"></script>
			<style type="text/css">
				.frame{
					padding-top: 50px;
				}
			</style>
		<?php elseif($this->uri->segment(2) == 'categories'):?>
			<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/catagories.css">

		<?php elseif($this->uri->segment(2)== 'items'): ?>
			<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/items.css');?>">
			<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/jquery.bxslider.css');?>">
		<?php elseif($this->uri->segment(2) == 'place_ad'): ?>
			<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/play-ad-catagories.css">
		
		<?php endif; ?>
		<?php if (!$this->ion_auth->logged_in()): ?>
			<script type="text/javascript">var users_fb_login_status = "<?php echo site_url('users/fb_login_status');?>";</script>
			<?php if($this->agent->is_mobile()):?>
				<script type="text/javascript">var user_profile = "<?php echo site_url('mobile/user_profile');?>";</script>
			<?php else:?>
				<script type="text/javascript">var user_profile = "<?php echo site_url('users/user_profile');?>";</script>			
		 	<?php endif;?>
			<script src="<?php echo base_url('js/fb_js.js'); ?>"></script>
			<style>
				.fb-login-button{
					cursor: pointer;
				}
			</style>
		<?php endif; ?>
	<style>
		#bs-example-navbar-collapse-1{
			background-color: <?php echo $home_menu_color;?>;
		}
	</style>
	</head>
	<body>
		<div class="row">
			<?php if (!$this->ion_auth->logged_in()): ?>
				<div class="col-md-12" id="signin">
					<div class="row">
						<div class="col-md-1 col-md-offset-2">
							<h2><?php echo $this->session->userdata('lang_current') == 'dr' ? 'ورود' : 'Sign in'; ?></h2>
							<h4 class="pull-right"><a href="<?php echo site_url('home/registration'); ?>"><?php echo $this->session->userdata('lang_current') == 'dr' ? 'ثبت نام' : 'or Register'; ?></a></h4>
						</div>
						<div class="col-md-3">
							<img data-original="<?php echo base_url(); ?>img/sign-in-with-fb.png" alt="" class="sigin-fb fb-login-button">
						</div>
						<div class="col-md-4 col-md-offset-1 sign-box">
							<?php echo form_open('users/login', 'class="form-inline" role="form"'); ?>
							
								<div class="form-group">
									<input type="email" class="form-control" name="email" id="exampleInputEmail2" placeholder="Enter email">
									<p><?php echo form_error('email'); ?></p>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" name="password" id="exampleInputPassword2" placeholder="Password">
									<p><?php echo form_error('password'); ?></p>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-danger"><?php echo $this->session->userdata('lang_current') == 'dr' ? 'ورود' : 'Sign in'; ?></button>
								</div>
								<br>
								<!-- link this to a controller/function which sets the default password and emails 
									the user on his email the default password and option to rest password -->
								<a href="<?php echo site_url('users/forget_password') ?>" class="btn btn-link">Forget Password</a>
							<?php echo form_close(); ?>
						</div>
						<div class="col-md-1">
							<span class="glyphicon glyphicon-off"></span>
						</div>
					</div>
				</div>
			<?php endif ?>
		</div>
