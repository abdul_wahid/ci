<div class="col-md-2">
	<h4><?php echo $this->session->userdata('lang_current') == 'dr' ? 'گذارش های اخیر' : 'Recent Updates'; ?></h4>
	<ul class="list-group">
		<?php if (count($recent_updates)): ?>
			<?php foreach ($recent_updates as $cate): ?>
				  <li class="list-group-item">
				  	<?php echo $cate->comp_name; ?>
				  </li>
			<?php endforeach;?>
		<?php endif ?>
		
	</ul>
	
</div>