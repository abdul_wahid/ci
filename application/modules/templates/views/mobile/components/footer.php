<footer class="row">
	<div class="col-xs-12" id="footer-buttons">
		<!-- $this->uri->segment(1). this should be used before every site_url -->
		<div class="col-xs-3"><a href="<?php echo site_url('mobile/index'); ?>" class="btn text-center"><span class="fa fa-home fa-2x"></span><br>Home</a></div>
		<div class="col-xs-3"><a href="<?php echo site_url('mobile/place_ad');?>" class="btn text-center"><span class="fa fa-tags fa-2x"></span><br><p style="margin-left: -15px;">Place an Ad</p></a></div>
		<div class="col-xs-3"><a href="<?php echo site_url('mobile/search');?>" class="btn text-center"><span class="fa fa-search fa-2x"></span><br><?php echo $this->session->userdata('lang_current') == 'dr' ? 'جستجو' : 'Search'; ?></a></div>
		<div class="col-xs-3"><a href="<?php echo site_url('mobile/setting');?>" class="btn text-center"><span class="fa fa-cog fa-2x"></span><br>Setting</a></div>
	</div>
	<div class="col-xs-12" id="footer-links">
		<?php if ($this->ion_auth->logged_in()): ?>
			<div class="col-xs-3"><a href="<?php echo site_url('mobile/user_profile'); ?>" class="btn text-center"><span class="fa fa-user fa-2x"></span><br>User Profile</a></div>
			<div class="col-xs-3"><a href="<?php echo site_url('users/logout'); ?>" class="btn text-center"><span class="fa fa-sign-out fa-2x"></span><br><?php echo $this->session->userdata('lang_current') == 'dr' ? 'خروج' : 'Logout'; ?></a></div>
		<?php else: ?>
			<div class="col-xs-3"><a href="<?php echo site_url('mobile/signin'); ?>" class="btn text-center"><span class="fa fa-sign-in fa-2x"></span><br><?php echo $this->session->userdata('lang_current') == 'dr' ? 'ورود' : 'Sign in'; ?></a></div>
			<div class="col-xs-3"><a href="#" class="btn text-center"><span class="fa fa-life-ring fa-2x"></span><br>Help</a></div>
		<?php endif ?>
		<div class="col-xs-3"><a href="<?php echo site_url('mobile/topup_companies'); ?>" class="btn text-center"><span class="fa fa-mobile fa-2x"></span><br><?php echo $this->session->userdata('lang_current') == 'dr' ? 'کارت موبائیل' : 'Top Up'; ?></a></div>
		<div class="col-xs-3"><a href="<?php echo site_url('mobile/quick_links'); ?>" class="btn text-center"><span class="fa fa-users fa-2x"></span><br>About</a></div>
	</div>
	<div class="col-xs-12">
		<p id="copy-right">Osyar.com &copy; <?php echo date('Y'); ?></p>
	</div>
</footer>
</div>
<!-- fluid container end -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/mobile/js/mobile_script.js"></script>


<script>
	$(document).ready(function() {
		jQuery(document).ready(function($) {
			$('.list-group-item').addClass('list-group-item-danger');
		});
	});

	$(function (){
		$(window).resize(function() {
			/* Act on the event */
			if(jQuery(window).width()>1024){
				// redirect to mobile site
				window.location.replace("<?php echo site_url('home/home'); ?>");
			}
		});
		if(jQuery(window).width()>1024){
			window.location.replace("<?php echo site_url('home/home'); ?>");
		}
	});
</script>
</body>
</html>