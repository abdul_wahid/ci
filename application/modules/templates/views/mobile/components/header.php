<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Osyar</title>
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/css/mobile_style.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<script src="<?php echo base_url(); ?>js/script.js"></script>
		<!-- <link href="<?php //echo base_url(); ?>assets/admin/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript" charset="utf-8">
			$(function() {
				$(document).ready(function() {
					$('#language > a').on('click', function(event) {
		  				event.preventDefault();
		  				
		  				// alert("<?php echo $this->session->userdata('lang_current');?>");
		  				// if($('#language').data('lang') == 'en'){
		  				// 	$('#language').data('lang', 'dr');
		  				// 	$('#language > a').text('Dari');
		  				// }
		  				// else if($('#language').data('lang') == 'dr'){
		  				// 	$('#language').data('lang', 'en');
		  				// 	$('#language > a').text('English');
		  				// }

		  				$.ajax({
		  					url: "<?php echo site_url('mobile/set_city_session');?>",
		  					type: 'POST',
		  					dataType: '',
		  					data: {lang: $('#language').data('lang')},
		  				})
		  				.done(function(data) {
		  					console.log("success");
		  					console.log(data);
		  					window.location.reload(true);
		  					// window.location.replace("<?php echo site_url('"+window.location.href+"'); ?>");
		  				})
		  				.fail(function() {
		  					console.log("error");
		  				});
		  				


		  			});
				});
			});
		</script>
	</head>
	<body>
		<!-- fluid container start -->
		<div class="fluid-container">
			<header class="row">
				<div class="col-xs-12">
					<div class="col-xs-3 col-xs-offset-4">
						<img src="<?php echo base_url(); ?>img/logo.gif" alt="logo" class="img-responsive logo">
					</div>
					<div class="col-xs-1 col-xs-offset-4">
						<a class="fancybox" href="<?php echo base_url(); ?>img/af.png" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
							<img src="<?php echo base_url(); ?>img/af.png" alt="flag" class="img-responsive flag pull-right">
						</a>
					</div>
				</div>
			</header>