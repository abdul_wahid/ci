<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables/media/css/jquery.dataTables.min.css'); ?>">
<script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/DataTables/media/js/jquery.dataTables.min.js'); ?>"></script>
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<h2><a href="<?php echo site_url('users/add'); ?>" class="btn btn-default"> Add Member</a>  <a href="<?php echo site_url('users/groups'); ?>" class="btn btn-default">Add Groups</a></h2>

			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="table-responsive">
					<table class="table table-hover table-striped table-bordered display" id="table_id">
						<thead>
							<tr>
								<th>User ID</th>
								<th>Frist Name</th>
								<th>Last Name</th>
								<th>Username</th>
								<th>Email</th>
								<th>User Group</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<?php if (count($users)>0): ?>
								<?php foreach ($users as $u): ?>
								<tr>
									<td><?php echo $u->id; ?></td>
									<td><?php echo $u->first_name; ?></td>
									<td><?php echo $u->last_name; ?></td>
									<td><?php echo $u->username; ?></td>
									<td><?php echo $u->email; ?></td>
									<td><?php echo $u->name; ?></td>
									<td><?php echo edit_btn(site_url('users/add/'.$u->edit)); ?></td>
									<td><?php echo delete_btn(site_url('users/delete/'.$u->delete)); ?></td>
								</tr>
								<?php endforeach ?>
							<?php else: ?>
								<p>No Users Added Yet!.</p>
							<?php endif ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	$(document).ready( function () {
	    $('#table_id').DataTable();
	} );
</script>