<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables/media/css/jquery.dataTables.min.css'); ?>">
<script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/DataTables/media/js/jquery.dataTables.min.js'); ?>"></script>
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<h2> Members Section <a href="<?php echo site_url('users/group_add'); ?>" class="btn btn-default"> Add Member</a>   <a href="<?php echo site_url('users/index'); ?>" class="btn btn-default"> Go Back</a></h2>
		</div>
	</div>

		<div class="row">
			<div class="col-md-12">
				<table class="table table-hover table-striped table-bordered display" id="table_id">
					<thead>
						<tr>
							<th>Group Name</th>
							<th>Group Description</th>
							<th>Group Delete</th>
						</tr>
					</thead>
					<tbody>
						<?php if (count($groups) > 0): ?>
							<?php foreach ($groups as $g): ?>
							<tr>
								<td><?php echo $g->name; ?></td>
								<td><?php echo $g->description; ?></td>
								<td><?php echo delete_btn(site_url('users/group_delete/'.$g->delete)); ?></td>
							</tr>
							<?php endforeach ?>
						<?php else: ?>
								<p>No Users Added Yet!.</p>
							<?php endif ?>
					</tbody>
				</table>
				
			</div>
		</div>
</div>

<script>
	$(document).ready( function () {
	   $('#table_id').DataTable();
	} );
</script>


