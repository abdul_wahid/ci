<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<h2> Profile <?php echo $this->uri->segment(3) != ''? 'Update' : 'Create'; ?> Section</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<?php echo form_open('','class="form-horizontal" role="form"');?>
					<div class="form-group">
					<label for="groups_id" class="col-sm-2 control-label">groups_id</label>
						<div class="col-sm-4">
							<?php echo form_error('groups_id', '<div class="text-danger">', '</div>'); ?>
							<select name="group_id" id="groups_id" class="form-control" required="required">
								<?php foreach ($groups as $g): ?>
									<option value="<?php echo $g->id; ?>"><?php echo ucfirst($g->name); ?></option>	
								<?php endforeach ?>
								
							</select>
							
						</div>
					</div>		

					<div class="form-group">
						<label for="first_name" class="col-sm-2 control-label">First Name</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo isset($user->first_name) == true ? $user->first_name : set_value('first_name'); ?>" placeholder="First Name">
								<?php echo form_error('first_name', '<div class="text-danger">', '</div>'); ?>
							</div>
						</div>

					<div class="form-group">
						<label for="last_name" class="col-sm-2 control-label">Last Name</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo isset($user->last_name) == true ? $user->last_name : set_value('last_name'); ?>" placeholder="Last Name">
								<?php echo form_error('last_name', '<div class="text-danger">', '</div>'); ?>
							</div>
						</div>						
						
					<div class="form-group">
						<label for="username" class="col-sm-2 control-label">Username</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="username" name="username" value="<?php echo isset($user->username) == true ? $user->username : set_value('username'); ?>" placeholder="Username">
								<?php echo form_error('username', '<div class="text-danger">', '</div>'); ?>
							</div>
						</div>		

						<div class="form-group">
						<label for="email" class="col-sm-2 control-label">Email</label>
							<div class="col-sm-4">
								<input type="email" class="form-control" id="email" name="email" value="<?php echo isset($user->email) == true ? $user->email : set_value('email'); ?>" placeholder="Email">
								<?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
							</div>
						</div>		

						<div class="form-group">
						<label for="password" class="col-sm-2 control-label">Password</label>
							<div class="col-sm-4">
								<input type="password" class="form-control" id="password" name="password" value="<?php echo isset($user->password) == true ? $user->password : set_value('password'); ?>" placeholder="Password">
								<?php echo form_error('password', '<div class="text-danger">', '</div>'); ?>
							</div>
						</div>						
						
				
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<button type="submit" name="submit" class="btn btn-primary btn-block"><?php echo $this->uri->segment(4) != ''? 'Update' : 'Save'; ?></button>
						</div>
					</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>