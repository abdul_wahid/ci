<style>
	tr>td:first-child{
		width: 20%;
	}
</style>

<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col md-12 col-sm-12">
				<h2><?php echo $this->ion_auth->group($this->uri->segment(3))->row()->description; ?>   	<a href="<?php echo site_url('users/add/'.$this->uri->segment(3)); ?>" class="btn btn-default"> Add Member</a>    <a href="<?php echo site_url('users/add/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>" class="btn btn-default"> Edit Member</a>   <a href="<?php echo site_url('users/index/'.$this->uri->segment(3)); ?>" class="btn btn-default"> Go Back</a></h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="table-responsive">
					<table class="table table-hover table-striped table-bordered">
						<thead>
							<tr>
								<th>Column</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody>

							<tr>
								<td>First Name</td>
								<td><?php echo $users->first_name ;?></td>
							</tr>
							<tr>
								<td>Last Name</td>
								<td><?php echo $users->last_name ;?></td>
							</tr>
							<tr>
								<td>Email</td>
								<td><?php echo $users->email ;?></td>
							</tr>
							<tr>
								<td>Username</td>
								<td><?php echo $users->username;?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>