<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<?php echo form_open('','class="form-horizontal" role="form"');?>
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">Group Name</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="name" name="name"  placeholder="Group Name">
								<?php echo form_error('name', '<div class="text-danger">', '</div>'); ?>
							</div>
						</div>

						<div class="form-group">
						<label for="description" class="col-sm-2 control-label">Group Description</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="description" name="description"  placeholder="Group Description">
								<?php echo form_error('description', '<div class="text-danger">', '</div>'); ?>
							</div>
						</div>
						</div>
						
				
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<button type="submit" name="submit" class="btn btn-primary btn-block">Save</button>
						</div>
					</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>