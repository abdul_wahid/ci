<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users_m');
	}

	public function index($groups_id = null)
	{
		$this->data['users'] = $this->users_m->users_index_get();
		// dump_exit($this->data['users']);
		$this->data['modules'] = 'users';
		$this->data['sub_view'] = 'index';

		echo Modules::run('templates/admin',$this->data);
	}

	public function login(){
		// dump_exit($_POST);
		if ($this->ion_auth->login($this->input->get_post('email'),$this->input->post('password'))) {
			redirect('admin/dashboard');
		}
		else{
			redirect('');
		}
	}
	public function logout(){
		$this->ion_auth->logout();
		redirect('');
	}

	public function add($user_id = null){
		
		if(!is_null($this->users_m->add($user_id))){
			redirect('users/index');
		}
		if(!is_null($user_id)){
			$this->data['user'] = $this->users_m->get($user_id);
			$this->data['groups'] = $this->users_m->get_users_groups($this->data['user']->group_id,TRUE);
			// dump($this->data['groups']);
			// dump_exit($this->data['user']);
		}
		else{
			$this->data['groups'] = $this->users_m->get_users_groups();
		}
		
		$this->data['modules'] = 'users';
		$this->data['sub_view'] = 'add';

		echo Modules::run('templates/admin',$this->data);
	}

	public function preview($user_id = null){
		$this->data['users'] = $this->users_m->get($user_id);
		// dump_exit($this->data['users']);
		$this->data['modules'] = 'users';
		$this->data['sub_view'] = 'preview';

		echo Modules::run('templates/admin',$this->data);
	}

	public function delete($user_id = null){
		$this->ion_auth->delete_user($user_id);
		redirect('users/index');
	}

	public function groups(){
		$this->db->select('name,description,id as "delete"');
		$this->data['groups'] = $this->db->get('groups')->result();
		// dump_exit($this->data['groups']);
		$this->data['modules'] = 'users';
		$this->data['sub_view'] = 'groups';

		echo Modules::run('templates/admin',$this->data);
	}

	public function group_delete(){
		exit('get details on how to remove the group and what happends to the people with in the gorup');
	}

	public function group_add(){
		if(!is_null($this->users_m->group_add())){
			redirect('users/index');
		}
		$this->data['modules'] = 'users';
		$this->data['sub_view'] = 'group_add';

		echo Modules::run('templates/admin',$this->data);
	}

}

/* End of file users.php */
/* Location: ./application/controllers/users.php */