<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_m extends CI_Model {

	private $tbl_name;

	public function __construct()
	{
		parent::__construct();
		$this->tbl_name = 'users';
		$this->load->library('form_validation');
	}

	public function users_index_get(){
		$sql ="
			SELECT u.id,u.first_name,u.last_name,u.username,u.email,g.name,u.id as 'edit', u.id as 'delete'
			from users as u
			left join users_groups as ug
			on u.id = ug.user_id
			left join groups as g
			on ug.group_id = g.id
		";
		return $this->db->query($sql)->result();
	}

	public function get($user_id = null){
		
		$sql= "
			select u.id,u.username,u.email,u.first_name,u.last_name ,ug.group_id
			from users as u 
			left join users_groups as ug 
			on u.id = ug.user_id 
		";
		if (!is_null($user_id) ) {
			$sql .= "where u.id = ".$user_id;
		}
		return $this->db->query($sql)->row();
	}

	public function add($user_id = null){

			
			// form validaiton
		if (isset($_POST['submit'])) {
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|xss_clean');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|xss_clean');
			
			if (!is_null($user_id)) {
				# updating user password check
				$this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|min_length[8]');
				$this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|required|valid_emails');
				$this->form_validation->set_rules('username', 'Username', 'trim|xss_clean|required');
			} else {
				# inserting user password check
				$this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|min_length[8]|required');
				$this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|required|is_unique[users.email]|valid_emails');
				$this->form_validation->set_rules('username', 'Username', 'trim|xss_clean|required|is_unique[users.username]');
			}
			
			
			if ($this->form_validation->run($this) == TRUE) {
									// exit('form validatied');
				$data = array(
						'first_name' => $this->input->post('first_name'),
						'last_name' => $this->input->post('last_name'),
					);
				if ($_POST['password'] != '') {
					$data['password'] = $this->input->post('password');
				}

				// saving the data
				if (is_null($user_id)) {
					# insert
					
					$email = $this->input->post('email');
					$password = $this->input->post('password');
					$group = array($this->input->post('group_id'));
					$data['username'] = $this->input->post('username');

					if ($this->ion_auth->register(null,$password,$email,$data,$group)) {
						redirect('users/index');

					}
				} else {
					# update
					if (!$this->ion_auth->is_admin()) {
						$user_id = $this->session->userdata('user_id');
					}
					$data['email'] = $this->input->post('email');
					$data['username'] = $this->input->post('username');
					// dump_exit($data);
					if($this->ion_auth->update($user_id,$data)){
						redirect('users/index/');
					}
				}
			} 
		}

		return null;
	}

	public function group_add(){
		if (isset($_POST['submit'])) {
			$this->form_validation->set_rules('name', 'Group Name', 'required|trim|xss_clean');
			$this->form_validation->set_rules('description', 'Group Description', 'trim|xss_clean');
			if ($this->form_validation->run($this) == TRUE) {
				$data =array(
						'name' => $this->input->post('name'),
						'description' => $this->input->post('description'),
					);
				if ($this->db->insert('groups',$data)) {
					redirect('users/groups');
				}
			}
		}
		return null;
	}

	/**
	 * This function returns the groups of the users or it returns a groups of the users by the order by id number 
	 * @param  Int  $group_id The id of number of the group
	 * @param  boolean $order_by return by the group_id number passed if it is true and vise versa
	 * @return STD:object            list of the names of the groups
	 */
	public function get_users_groups($group_id = null,$order_by = false){
		$this->db->select('id,name');
		if ($order_by) {
			$this->db->order_by('id = '.$group_id, 'desc');
		}
		return $this->db->get('groups')->result();
	}

}

/* End of file users_m.php */
/* Location: ./application/models/users_m.php */
