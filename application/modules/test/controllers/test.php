<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$browser = new Buzz\Browser();
		$response = $browser->get('http://www.google.com');

		dump_exit($response);
		echo $browser->getLastRequest()."\n";
		echo $response;
	}

	public function faker(){
		$faker = Faker\Factory::create();
		for($i=0; $i<10;$i++){
			echo $faker->time . "<br>";
		}
	}

}

/* End of file test.php */
/* Location: ./application/controllers/test.php */