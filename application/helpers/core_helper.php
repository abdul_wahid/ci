<?php
/**
 * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
 * @author Joost van Veen
 * @version 1.0
 */
if (!function_exists('dump')) {
    function dump ($var, $label = 'Dump', $echo = TRUE)
    {
        // Store dump in variable 
        ob_start();
        var_dump($var);
        $output = ob_get_clean();
        
        // Add formatting
        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';
        
        // Output
        if ($echo == TRUE) {
            echo $output;
        }
        else {
            return $output;
        }
    }
}


if (!function_exists('dump_exit')) {
    function dump_exit($var, $label = 'Dump', $echo = TRUE) {
        dump ($var, $label, $echo);
        exit;
    }
}

function edit_btn($uri){
    return anchor($uri, '<i class="glyphicon glyphicon-edit btn btn-success"></i>');
}

function gallery_btn($uri){
    return anchor($uri, '<i class="glyphicon glyphicon-picture btn btn-default"></i>');
}

/**
 * create an preview link for the passed uri
 * @param  int $uri the value for which the preview link is required
 * @return anchor_tag      returns and anchor which is bootstrap styled with the appropreate link
 */
function preview_btn($uri){
    return anchor($uri, '<i class="glyphicon glyphicon-eye-open btn btn-warning"></i>');
}

/**
 * create an delete link for the passed uri
 * @param  int $uri the value for which the delete link is required
 * @return anchor_tag      returns and anchor which is bootstrap styled with the appropreate link
 */
function delete_btn($uri){
    return anchor($uri, '<i class="glyphicon glyphicon-remove btn btn-danger"></i>', array(
        'onclick' => "return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
    ));
}

function add_product($uri){
    return anchor($uri, '<i class="glyphicon glyphicon-plus btn btn-primary"></i>');
}

function add_sub($uri){
    return anchor($uri, '<i class="glyphicon glyphicon-th-list btn btn-default"></i>');
}

